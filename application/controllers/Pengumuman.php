<?php

class Pengumuman extends CI_Controller{
    public function __construct(){
        parent:: __construct();
    }
    public function index(){
        $this->load->view('header/header');
        $this->load->view('isi/pengumuman');
        $this->load->view('header/footer');
    }
}